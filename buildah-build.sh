#!/bin/bash

# Based on: https://github.com/containers/buildah/blob/main/docs/tutorials/01-intro.md#building-a-container-from-scratch

echo "Setting up..."
export container_name='oci-rcm'
export build_container=$(buildah from scratch)
export releasever=$(rpm -E %fedora)

echo "Mounting image filesystem..."
buildah unshare
mountpath=$(buildah mount $build_container)

echo "Installing packages..."
dnf install --releasever $releasever --installroot $mountpath rcm --setopt install_weak_deps=false -y

echo "Configuring image..."
buildah config --cmd /usr/bin/rcup $container_name
buildah config --created-by "unshippedreminder"  $build_container
buildah config --author "https://gitlab.com/unshippedreminder/" --label name=oci-rcm $build_container

echo "Commiting image..."
buildah commit $build_container ${CI_PROJECT_NAME}:latest
