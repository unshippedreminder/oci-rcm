# OCI RCM image

A minimal OCI image with only [rcm](https://thoughtbot.github.io/rcm/rcm.7.html) (and dependencies) installed.

Runs `mkrc` by default.

## Usage

For arguments, see [`mkrc`](https://thoughtbot.github.io/rcm/mkrc.1.html) documentation.

TODO: Add instructions for running the image (mounting $HOME)

## Commands

[`mkrc`](https://thoughtbot.github.io/rcm/mkrc.1.html)

[`rcup`](https://thoughtbot.github.io/rcm/rcup.1.html)

[`rcdn`](https://thoughtbot.github.io/rcm/rcdn.1.html)

[`lsrc`](https://thoughtbot.github.io/rcm/lsrc.1.html)
